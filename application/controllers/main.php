<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {

	public function index()
	{
		$this->_set('page_header', 'This is a title!');
		$this->_set('hello', 'Hello World!');
	}
}
