<?php
/**
* A base controller for CodeIgniter with view autoloading, layout support,
* model loading, helper loading, asides/partials and per-controller 404
*
* @link http://github.com/jamierumbelow/codeigniter-base-controller
* @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
*/

class MY_Controller extends CI_Controller
{

  /* --------------------------------------------------------------
  * VARIABLES
  * ------------------------------------------------------------ */

  /**
  * Template file extension
  */
  protected $ext = '.twig';

  /**
  * The current request's view. Automatically guessed
  * from the name of the controller and action
  */
  protected $view = '';

  /**
  * An array of variables to be passed through to the
  * view, layout and any asides
  */
  protected $data = array();

  /**
  * The name of the layout to wrap around the view.
  */
  protected $layout;

  /**
  * An arbitrary list of asides/partials to be loaded into
  * the layout. The key is the declared name, the value the file
  */
  protected $asides = array();

  /**
  * A list of models to be autoloaded
  */
  protected $models = array();

  /**
  * A formatting string for the model autoloading feature.
  * The percent symbol (%) will be replaced with the model name.
  */
  protected $model_string = '%_model';

  /**
  * A list of helpers to be autoloaded
  */
  protected $helpers = array();

  /* --------------------------------------------------------------
  * GENERIC METHODS
  * ------------------------------------------------------------ */

  /**
  * Initialise the controller, tie into the CodeIgniter superobject
  * and try to autoload the models and helpers
  */
  public function __construct()
  {
    parent::__construct();

    $this->_load_models();
    $this->_load_helpers();
  }

  public function _set($key, $value)
  {
    $this->data[$key] = $value;
  }

  /* --------------------------------------------------------------
  * VIEW RENDERING
  * ------------------------------------------------------------ */

  /**
  * Override CodeIgniter's despatch mechanism and route the request
  * through to the appropriate action. Support custom 404 methods and
  * autoload the view into the layout.
  */
  public function _remap($method)
  {
    if (method_exists($this, $method))
    {
      call_user_func_array(array($this, $method), array_slice($this->uri->rsegments, 2));
    }
    else
    {
      if (method_exists($this, '_404'))
      {
        call_user_func_array(array($this, '_404'), array($method));
      }
      else
      {
        show_404(strtolower(get_class($this)).'/'.$method);
      }
    }

    $this->_load_view();
  }

  /**
  * Automatically load the view, allowing the developer to override if
  * he or she wishes, otherwise being conventional.
  */
  protected function _load_view()
  {
    $view = (!empty($this->view)) ? $this->view : $this->router->directory . $this->router->class . '/' . $this->router->method . $this->ext;
    $this->twig->display($view, $this->data);
  }

  /* --------------------------------------------------------------
  * MODEL LOADING
  * ------------------------------------------------------------ */

  /**
  * Load models based on the $this->models array
  */
  private function _load_models()
  {
    foreach ($this->models as $model)
    {
      $this->load->model($this->_model_name($model), $model);
    }
  }

  /**
  * Returns the loadable model name based on
  * the model formatting string
  */
  protected function _model_name($model)
  {
    return str_replace('%', $model, $this->model_string);
  }

  /* --------------------------------------------------------------
  * HELPER LOADING
  * ------------------------------------------------------------ */

  /**
  * Load helpers based on the $this->helpers array
  */
  private function _load_helpers()
  {
    foreach ($this->helpers as $helper)
    {
      $this->load->helper($helper);
    }
  }
}
